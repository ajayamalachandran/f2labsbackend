import { Component, OnInit } from '@angular/core';
import { CrudserviceService } from 'src/app/services/crudservice.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  exams=[];
  subjects=[];
  topics=[];
  chapters=[];
  showSubject:boolean=false;
  showtopic:boolean=false;
  showChapter:boolean=false;
  showQuestions:boolean=false;
  topicCardVal;
  subjectCardVal;
  examCardVal;
  chapterCardVal;
  

  constructor(private crudService:CrudserviceService,private http:HttpClient) { }

  ngOnInit() {
    this.getExams() 
  }
  save(type,value){
    if(type==='exam'){
      this.crudService.create('/exam/create',{examName:value}).subscribe((data)=>{
        if(data['data']){
          this.getExams();
        }
      })
    }else if(type==='subject'){
      if(this.examCardVal!==undefined && this.examCardVal!==null){
        let tmpObj={
          subjectName:value,
          exam:this.examCardVal['id']
        }
        this.crudService.create('/subject/create',tmpObj).subscribe((data)=>{
          if(data['data']){
            this.getSubjects(this.examCardVal);
          }
        })
      }
    }else if(type==='topic'){
      if(this.subjectCardVal!==undefined && this.subjectCardVal!==null){
        let tmpTopicObj={
          topicName:value,
          exam:this.subjectCardVal['exam']['id'],
          subject:this.subjectCardVal['id']
        }
        this.crudService.create('/topic/create',tmpTopicObj).subscribe((data)=>{
          if(data['data']){
            this.getTopics(this.subjectCardVal);
          }
        })
      }
    }
    else if(type==='chapter'){
      if(this.topicCardVal!==undefined && this.topicCardVal!==null){
        let tmpChapterObj={
          chapterName:value,
          exam:this.topicCardVal['exam']['id'],
          subject:this.topicCardVal['subject']['id'],
          topic:this.topicCardVal['id']
        }
        console.log(this.topicCardVal);
        this.crudService.create('/chapter/create',tmpChapterObj).subscribe((data)=>{
          if(data['data']){
            this.getTopics(this.subjectCardVal);
          }
        })
      }
    }
  
  }
  getExams(){
    this.exams=[];
    this.crudService.read('/exam').subscribe((res)=>{
      if(res['result']){
        this.exams=res['result']
      }
    })
  }
  getSubjects(cardVal){
    this.subjects=[];
    this.crudService.read('/subject',{ params:{
      exam:cardVal['id']
    }
  }).subscribe((res)=>{
    if(res['result']){
      this.subjects=res['result']
    }
  })
  }
  getTopics(cardVal){
    this.topics=[];
    this.crudService.read('/topic',{ params:{
      exam:cardVal['exam']['id'],
      subject:cardVal['id']
    }
  }).subscribe((res)=>{
    if(res['result']){
      console.log(res['result']);
      this.topics=res['result']
    }
  })
  }
  getChapters(cardVal){
    this.chapters=[];
    this.crudService.read('/chapter',{ params:{
      exam:cardVal['exam']['id'],
      subject:cardVal['subject']['id'],
      topic:cardVal['id']
    }
  }).subscribe((res)=>{
    if(res['result']){
      console.log(res['result']);
      this.chapters=res['result']
    }
  })

  }
  onSelectCard(type,card){
    if(type==='exam'){
      this.subjects=[];
      this.examCardVal=card;
      this.showSubject=true;
      this.showtopic=false;
      this.showChapter=false;

      this.getSubjects(card);
    }else if(type==='subject'){
      this.topics=[];
      this.subjectCardVal=card;
      this.showtopic=true;
      this.showChapter=false
      this.getTopics(card);
    }else if(type==='topic'){
      this.chapters=[];
      console.log(card);
      this.topicCardVal=card;
      this.showtopic=true;
      this.showChapter=true;
      this.getChapters(card);
    }else if(type==='chapter'){
      console.log(card)
      this.chapterCardVal=card;
      this.showQuestions=true;
    }
  }
  files;
  onSelectFile(evt){
    this.files=evt.target.files[0];
  }
  convertedQuestions=[];
  uploadQuestions(){
if(this.files!==undefined){
  let formData =new FormData();
  formData.append('files',this.files);
  this.http.post('/question/convert',formData).subscribe((data)=>{
    if(data['jsonObj']){
      if(this.chapterCardVal!==undefined){
        console.log(this.chapterCardVal)
        this.convertedQuestions=data['jsonObj'];
        this.convertedQuestions.map((item)=>{
          item['exam']=this.chapterCardVal['exam']['id']
          item['subject']=this.chapterCardVal['subject']['id']
          item['topic']=this.chapterCardVal['topic']['id']
          item['chapter']=this.chapterCardVal['id']
          return item;
        })
        console.log(this.convertedQuestions);
        this.crudService.create('/question/create',this.convertedQuestions).subscribe((data)=>{
          console.log(data);
        })
      }
     
    }
  })
  // this.crudService.create('/question/create',formData).subscribe((data)=>{
  //   console.log(data)
  // })
}
  }

}
