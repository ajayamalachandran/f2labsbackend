import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { CrudserviceService} from '../../../services/crudservice.service';
import {TokenServiceService} from '../../../services/token-service.service';
import {Router } from '@angular/router';



@Component({
  selector: 'app-authendication',
  templateUrl: './authendication.component.html',
  styleUrls: ['./authendication.component.css']
})
export class AuthendicationComponent implements OnInit {
  loginForm:FormGroup;

  constructor(private fb:FormBuilder,private crudService:CrudserviceService,private tokenService:TokenServiceService,private route:Router) {
   }

  ngOnInit() {
    this.loginForm= this.fb.group({
      email: ['',[Validators.email,Validators.required]],
      password:['',[Validators.required,Validators.min(8)]]
    });

  }
  login(loginForm){
    if(this.loginForm.invalid){
      return
    }else {
      this.crudService.create('/user/signin',loginForm.value).subscribe((data)=>{
        console.log(data);
        if(data['user']['role']===1){
          this.route.navigate(['student-dashboard']);
          this.tokenService.setToken(data);
        }else {
          this.tokenService.setToken(data);
          this.route.navigate(['admin-dashboard']);
        }
      })
    }
  }
  signup(){
    this.route.navigate(['register']);
  }

}
