import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { CrudserviceService} from '../../../services/crudservice.service';
import {TokenServiceService} from '../../../services/token-service.service';
import {Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private fb:FormBuilder,private crudService:CrudserviceService,private tokenService:TokenServiceService,private route:Router) { }
  registerForm:FormGroup;
  ngOnInit() {
    this.registerForm= this.fb.group({
      firstName:['',Validators.required],
      email: ['',[Validators.email,Validators.required]],
      password:['',[Validators.required,Validators.min(8)]],
      role:[1]
    });
  }
  signUp(form){
    this.crudService.create('/user/signup',this.registerForm.value).subscribe((data)=>{
      if(data['result']){
        this.route.navigate(['login']);
      }
    })
  }
  signIn(){
    this.route.navigate(['login']);
  }

}
