import { Component, OnInit } from '@angular/core';
import {TokenServiceService} from '../../services/token-service.service';
import {CrudserviceService} from '../../services/crudservice.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private tokenService:TokenServiceService,private crudService:CrudserviceService,private route:Router) { }

  ngOnInit() {
  }
  logout(){
    var currentToken = this.tokenService.getToken();
    // console.log(currentToken);
    this.crudService.create('/user/signout', currentToken).subscribe(data => {
      if (data) {
        this.route.navigate(['/login']);
        this.tokenService.setToken(null);
      }
    })
  }

}
