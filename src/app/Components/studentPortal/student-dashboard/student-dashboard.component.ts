import { Component, OnInit } from '@angular/core';
import { TokenServiceService } from 'src/app/services/token-service.service';
import { CrudserviceService } from 'src/app/services/crudservice.service';
import { Router } from '@angular/router';
import { PopupComponent } from 'src/app/popup/popup.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-student-dashboard',
  templateUrl: './student-dashboard.component.html',
  styleUrls: ['./student-dashboard.component.css']
})
export class StudentDashboardComponent implements OnInit {
  exams = [];
  subjects = [];
  topics = [];
  chapters = [];
  showSubject: boolean = false;
  showtopic: boolean = false;
  showChapter: boolean = false;
  showQuestionCatogory: boolean = false;
  topicCardVal;
  subjectCardVal;
  examCardVal;
  chapterCardVal;
  questionCatogory = [
    { value: 1, name: 'Easy' },
    { value: 2, name: 'Medium' },
    { value: 3, name: 'Hard' }
  ]

  constructor(
    private tokenService: TokenServiceService,
    private crudService: CrudserviceService,
    private route: Router,
    private dialog:MatDialog
  ) { }

  ngOnInit() {
    this.getExams()
  }

  getExams() {
    this.exams = [];
    this.crudService.read('/exam').subscribe((res) => {
      if (res['result']) {
        this.exams = res['result']
      }
    })
  }
  getSubjects(cardVal) {
    this.subjects = [];
    this.crudService.read('/subject', {
      params: {
        exam: cardVal['id']
      }
    }).subscribe((res) => {
      if (res['result']) {
        this.subjects = res['result']
      }
    })
  }
  getTopics(cardVal) {
    this.topics = [];
    this.crudService.read('/topic', {
      params: {
        exam: cardVal['exam']['id'],
        subject: cardVal['id']
      }
    }).subscribe((res) => {
      if (res['result']) {
        console.log(res['result']);
        this.topics = res['result']
      }
    })
  }
  getChapters(cardVal) {
    this.chapters = [];
    this.crudService.read('/chapter', {
      params: {
        exam: cardVal['exam']['id'],
        subjects: cardVal['subject']['id'],
        topics: cardVal['id']
      }
    }).subscribe((res) => {
      if (res['result']) {
        this.chapters = res['result']
      }
    })

  }
  onSelectCard(type, card) {
    this.questionType = {};
    if (type === 'exam') {
      this.subjects = [];
      this.questionType = {
        exam: card['id']
      }
      this.examCardVal = card;
      this.showSubject = true;
      this.showtopic = false;
      this.showChapter = false;

      this.getSubjects(card);
    } else if (type === 'subject') {
      this.topics = [];
      this.subjectCardVal = card;
      this.showtopic = true;
      this.showChapter = false
      this.questionType = {
        exam: card['exam']['id'],
        subject: card['id']
      }
      this.getTopics(card);
    } else if (type === 'topic') {
      this.chapters = [];
      this.topicCardVal = card;
      this.showtopic = true;
      this.showChapter = true;
      this.getChapters(card);
      this.questionType = {
        exam: card['exam']['id'],
        subject: card['subject']['id'],
        topic: card['id']
      }
    } else if (type === 'chapter') {
      this.chapterCardVal = card;
      this.questionType = {
        exam: this.chapterCardVal['exam']['id'],
        subject: this.chapterCardVal['subject']['id'],
        topic: this.chapterCardVal['topic']['id'],
        chapter: this.chapterCardVal['id'],
      }
      this.showQuestionCatogory = true;
    }
  }
  convertedQuestions = [];
  logout() {
    var currentToken = this.tokenService.getToken();
    // console.log(currentToken);
    this.crudService.create('/user/signout', currentToken).subscribe(data => {
      if (data) {
        this.route.navigate(['/login']);
        this.tokenService.setToken(null);
      }
    })
  }
  selectedCatogoryVal;
  testQuestions = [];
  questionType = {};
  selectedCatagory(Catvalue) {
    this.selectedCatogoryVal = Catvalue;
    this.questionType = {
      exam: this.chapterCardVal['exam']['id'],
      subject: this.chapterCardVal['subject']['id'],
      topic: this.chapterCardVal['topic']['id'],
      chapter: this.chapterCardVal['id'],
      questionType: this.selectedCatogoryVal
    }
    this.crudService.read('/question', { params: this.questionType }).subscribe((data) => {
      if (data['result']) {
        this.testQuestions = data['result'];
      }
    })
    console.log(Catvalue);
  }
  startTest() {
    // this.questionType['testStatus']=true;
    this.questionType['user'] = this.tokenService.getToken()['user']['id'];
    // this.crudService.read('/question',{params:this.questionType}).subscribe((data)=>{
    // console.log(data)
    // if(data['result']){
    this.route.navigate(['task'], { queryParams: this.questionType });
    // }
    // })
  }
  onStartTestInfo() {
    this.crudService.read('/question', { params: this.questionType }).subscribe((data) => {
      if (data['result']) {
        this.testQuestions = data['result'];
      }
    })
  }
  chartValue={}
  cartIndex=0;
  getReport(params,index) {
    this.crudService.read('/report', { params: params }).subscribe((data) => {
      if (data !== undefined && data !== null) {
        if (data['result']) {
          this.cartIndex=index;
          this.chartValue=data['result'];
          this.openDialog(this.chartValue)
        }
      }
    })
  }
  getExamReport(exam,index) {
    var params = {}
    params['report'] = 'exam';
    params['exam'] = exam['id']
    this.getReport(params,index);

  }
  getSubjectReport(subject,index) {
    var params = {}
    params['report'] = 'subject';
    params['subject'] = subject['id'];
    console.log(params)
    this.getReport(params,index);

  }
  getTopicReport(topic,index) {
    var params = {}
    params['report'] = 'topic';
    params['topic'] = topic['id']
    this.getReport(params,index);

  }

  getChapterReport(chapter,index) {
    var params = {}
    params['report'] = 'chapter';
    params['chapter'] = chapter['id']
    this.getReport(params,index);

  }

  openDialog(data): void {
    const dialogRef = this.dialog.open(PopupComponent, {
      width: '400px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
