import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import { CrudserviceService } from '../../../services/crudservice.service';
import {TokenServiceService} from '../../../services/token-service.service';

@Component({
  selector: 'app-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.css']
})
export class TestPageComponent implements OnInit {
  questionParams={};
  questions=[];
  selectedQuestionIndex=0;

  constructor(private activeRoute:ActivatedRoute,private route:Router,private crudService:CrudserviceService,private tokenService:TokenServiceService) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe((params)=>{
      this.questionParams=Object.assign({},params)
      this.questionParams['user']=this.tokenService.getToken()['user']['id']
      this.questionParams['testStatus']=true;
      this.getQuestions(this.questionParams);
    })
  }
  onSelectAnswer(evt,question,index){
    console.log(index,question,evt)
    let answerObject={
      id:question['id'],
      answer:evt.value,
      user:this.tokenService.getToken()['user']['id']
    }
    if(answerObject!==undefined && answerObject!==null){
      this.crudService.create('/studentmeta/check',answerObject).subscribe((data)=>{
        // console.log(data)
        if(data){
          console.log('here');
          this.questionParams['question']=question['id'];
          this.getQuestions(this.questionParams);
        }
      })
    }
  }
  getQuestions(params){
    this.crudService.read('/question',{params:params}).subscribe((data)=>{
      if(data['result']){
        this.questions=data['result'];
        if(this.questions.length===0){

        }
      }
    })
  }
  getReport(){
    // var params=this.questionParams;
    // params['report']='exam';
    // this.crudService.read('/report',{params:this.questionParams}).subscribe((data)=>{
    //   console.log(data)
    // })
    this.route.navigate(['student-dashboard']);
  }
  onGoNext(index){
    console.log(index)
    // this.selectedQuestionIndex=0;
    this.selectedQuestionIndex=index<=this.questions.length-1 ? index + 1 : this.selectedQuestionIndex;
  }
  onGoPrevious(index){
    this.selectedQuestionIndex=index > 0 ? index - 1 : this.selectedQuestionIndex;
  }
}
