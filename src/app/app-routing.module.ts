import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthendicationComponent} from './Components/authendication/login/authendication.component';
import { StudentDashboardComponent } from './Components/studentPortal/student-dashboard/student-dashboard.component';
import { RegisterComponent } from './Components/authendication/register/register.component';
import { AdminDashboardComponent } from './Components/adminPortal/admin-dashboard/admin-dashboard.component';
import { TestPageComponent } from './Components/studentPortal/test-page/test-page.component';


const routes: Routes = [
  {
    path:'login',component:AuthendicationComponent},
    {path:'student-dashboard',component:StudentDashboardComponent},
    {path:'admin-dashboard',component:AdminDashboardComponent},
    {path:'register',component:RegisterComponent},
    {path:'task',component:TestPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
