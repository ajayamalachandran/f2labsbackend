import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthendicationComponent } from './Components/authendication/login/authendication.component';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatExpansionModule} from '@angular/material/expansion';
import { ReactiveFormsModule } from '@angular/forms';
import {HTTP_INTERCEPTORS,HttpClientModule} from '@angular/common/http';
import { ApiInterceptorService } from './services/api-interceptor.service';
import { RegisterComponent } from './Components/authendication/register/register.component';
import { StudentDashboardComponent } from './Components/studentPortal/student-dashboard/student-dashboard.component';
import { AdminDashboardComponent } from './Components/adminPortal/admin-dashboard/admin-dashboard.component';
import {MatButtonModule} from '@angular/material/button';
import { TestPageComponent } from './Components/studentPortal/test-page/test-page.component';
import { HeaderComponent } from './Components/header/header.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import { PopupComponent } from './popup/popup.component';
import {MatDialogModule} from '@angular/material/dialog';




@NgModule({
  declarations: [
    AppComponent,
    AuthendicationComponent,
    RegisterComponent,
    StudentDashboardComponent,
    AdminDashboardComponent,
    TestPageComponent,
    HeaderComponent,
    PopupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
    FormsModule,
    MatListModule,
    MatButtonModule,
    MatRadioModule,
    MatExpansionModule,
    FlexLayoutModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule
  ],
  entryComponents: [
    PopupComponent,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ApiInterceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
