import { Injectable } from '@angular/core';
import {HttpRequest,HttpResponse,HttpEvent,HttpHandler, HttpInterceptor, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {TokenServiceService} from './token-service.service';

@Injectable({
  providedIn: 'root'
})
export class ApiInterceptorService implements HttpInterceptor {
  token;
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.tokenService.getToken()!== null && this.tokenService.getToken()!== undefined){
      this.token=this.tokenService.getToken()['encodedToken'];
    }else {
      this.token=null;
    }
    let headers:HttpHeaders=new HttpHeaders({
      Authorization:`${this.token}`
    })
    console.log(headers)
    const request=req.clone({
      url:`${environment.backendUrl}${req.url}`,
      headers
    })
    return next.handle(request)

    // throw new Error("Method not implemented.");
  }
  

  constructor(private tokenService:TokenServiceService) { }
}
