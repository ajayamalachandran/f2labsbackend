import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class CrudserviceService {

  constructor(
    private http:HttpClient
  ) { }

  create(path:string,body:Object){
    return new Observable<any>((observable)=>{
      return this.http.post(path,body).subscribe((data)=>{
        observable.next(data)
      },error =>{
        observable.next(error)
      },()=>{
        observable.unsubscribe();
      })
    })
    
  }
  read(path:string,params?:Object){
    return new Observable<any>((observable)=>{
      return this.http.get(path,params).subscribe((data)=>{
        observable.next(data)
      },error =>{
        observable.next(error)
      },()=>{
        observable.unsubscribe();
      })
    })
    
  }
}
